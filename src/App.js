import React, { Component } from 'react';
import JsonData from "./jsonData.json";

class Magic extends Component{
  constructor(props) {
    super(props);
    this.state = {
     linered: false,
   };
  }
  //state
  stateRed = (event) =>{
    let a = !this.state.linered;
    this.setState({linered: a});
  }
render(){
  let {name} = this.props.item;
  return(
      <div className={this.state.linered ? 'lred' : 'lblue'}>{name}
        <button onClick={this.stateRed}>knopka</button>
      </div>
  )
}
}
//


class App extends Component {
    constructor(props) {
      super(props);
      this.state = {
       contacts: JsonData,
     };


    }

    handleChange = (event) => {
      let searchQuery = event.target.value.toLowerCase();
      let renderContacts = JsonData.filter( (item) => {
        let itemName = item.name.toLowerCase();
        return itemName.indexOf(searchQuery) !== -1;
      });
      this.setState({contacts: renderContacts});
      console.log( renderContacts);
    }

  render() {
    let {contacts} = this.state;
    return (
      <div className="App">
          <input onChange={this.handleChange}/>
          {
            contacts.map( (item,num)=> {
              return (
                  <Magic  key={num} item={item} />
              );
            })
          }
      </div>
    );
  }
}

export default App;
